import PriorityQueue from './priority-queue.js';

const c = 10; // tamano de las celdas canvas


let ctx;
const styles = ["red", "blue", "white", "black", "aqua", "teal"];
const renderCache = [];  //frames que faltan animar
let expandido = 0;
const randomAze = false;

const Estado = {
    Default: 0,
    Visitado: 1,
    Camino: 2,
    Pared: 3,
    Open: 4,
    Close: 5
}
const Direction = {
    Up: 0,
    Left: 1,
    Down: 2,
    Right: 3,
    Default: NaN
}

const main = () => {
    let opcion = prompt('MENU\n 1-Busqueda en profundidad\n 2-Busqueda en profundidad limitada\n 3-Busqueda A* \n ingrese el numero de la accion a realizar');
    const N = prompt('ingrese el valor de N');
    const obst = prompt('ingrese el porcentaje de casillas con obstaculos');
    let start;
    let end;
    let cantNodos;
    let ob;
    const w = c * ((N * 2) + 1); // ancho y alto del canvas
    const canvas = setupCanvas(w, w);
    document.body.appendChild(canvas);
    ctx = canvas.getContext('2d');

    let maze = initMaze(N, N);
    let entrada
    let salida
    if(randomAze){

        entrada = {x: 0, y: 0};
        salida = {x: maze.length - 1, y: maze[0].length - 1};
        ob = Math.round(100 / obst);
        randomMaze(maze, ob);
    }else{
        entrada = {x: 1, y: 1};
        salida = {x: maze.length - 2, y: maze[0].length - 2};
        perfectMaze(maze);
        generateMaze(maze);
        cleanMaze(maze);
    }



    draw(maze, ctx);

    switch (opcion) {
        case '1':
            start = Date.now();
            dfs(maze, entrada, salida);
            end = Date.now();
            console.log("tiempo de ejecucion:", end - start);
            break;
        case '2':
            const limite = prompt('ingrese el valor de casillas libres');
            start = Date.now();
            dfs(maze, entrada, salida, limite);
            end = Date.now();
            console.log(end - start);
            break;
        case '3':
            maze[1][1] = 0
            maze[maze.length - 2][maze[0].length - 2] = 1
            start = Date.now();
            busquedaA(maze, entrada, salida, distanciaManhattan);
            end = Date.now();
            console.log(end - start);
            break;
        default:
            opcion = prompt('MENU\n 1-Busqueda en profundidad\n 2-Busqueda en profundidad limitada\n 3-Busqueda A* \n ingrese el numero de la accion a realizar');
            break;
    }

};

/**
 *
 * @param maze
 * @param entrada posicion de la entrada
 * @param salida  objetivo.
 * @param heuristica referencia a funcion heuristica a utilizar.
 */
const busquedaA = (maze = [[]], entrada = {x: 1, y: 1}, salida = {
    x: maze.length - 2,
    y: maze[0].length - 2
}, heuristica = distanciaManhattan) => {
    // se inicializa una lista de nodos por visitar por medio de una cola de prioridad
    // se define el comparador de la cola de prioridad por medio de sus pesos f, f menor tiene mayor prioridad.
    const open = new PriorityQueue([], (a, b) => a.f - b.f);

    const celdaEntrada = {
        x: entrada.x,
        y: entrada.y,
        f: 0,
        g: 0,
        h: 0
    }  //se inicializa la celda de entrada con F=0
    open.enqueue(celdaEntrada);
    const closed = {};
    closed[entrada.x] = {};
    closed[entrada.x][entrada.y] = null;

    while (open.size > 0) { //mientras se tengan elementos por expandir

        const {x, y, g} = open.dequeue(); //se extrae el elemento con el menor valor de F
        closed[x][y] = Estado.Close;    //se marca como cerrado el nodo a expandir
        if ((x !== entrada.x && y !== entrada.y) && (x !== entrada.x && y !== entrada.y)) { // para no marcar la entrada ni la salida
            maze[x][y] = Estado.Close;
            renderCache.push({y, x, estado: Estado.Close});
        }
        expandido++;
        if (!(x === salida.x && y === salida.y)) { //si la celda actual no es la salida.
            const vecinos = generarSucesores(maze, x, y);
            vecinos.forEach(vecino => {
                const {x, y} = vecino;
                const vecinoG = g + 1;
                const h = round(heuristica({x, y}, {x: salida.x, y: salida.y}));
                const f = round(vecinoG + h);

                if (closed[x] === undefined) closed[x] = {};
                if (closed[x][y] === undefined) closed[x][y] = Estado.Default;

                if (closed[x][y] === Estado.Default) {
                    open.enqueue({x, y, f, g: vecinoG, h})   //se encola la posicion
                    maze[x][y] = Estado.Open;
                    renderCache.push({y, x, estado: Estado.Open});
                }

            });
        } else {
            break
        }

    }
}

const generarSucesores = (maze, x, y) => {
    const sucesores = [];
    if (x > 0 && maze[x - 1][y] !== Estado.Pared) {  //revisar arriba
        sucesores.push({x: x - 1, y});
    }
    if (y > 0 && maze[x][y - 1] !== Estado.Pared) {  //revisar izquierda
        sucesores.push({x, y: y - 1});
    }
    if (x < maze.length - 1 && maze[x + 1][y] !== Estado.Pared) {  //revisar abajo
        sucesores.push({x: x + 1, y});
    }
    if (y < maze[0].length - 1 && maze[x][y + 1] !== Estado.Pared) {  //revisar derecha
        sucesores.push({x, y: y + 1});
    }
    return sucesores;
}


const dfs = (maze = [[]], entrada = {x: 0, y: 0}, salida = {
    x: maze.length - 1,
    y: maze[0].length - 1
}, limit = Number.MAX_SAFE_INTEGER) => {
    const visitado = {};
    visitado[entrada.x] = {};
    visitado[entrada.x][entrada.y] = null;
    const pila = [];


    // colocamos la entrada como primer elemento de la pila
    pila.push({x: entrada.x, y: entrada.y, g: limit});

    while (pila.length > 0) {
        const {x, y, g} = pila.pop();
        visitado[x][y] = Estado.Close;    //se marca como cerrado el nodo a expandir
        if ((x !== entrada.x && y !== entrada.y) && (x !== entrada.x && y !== entrada.y)) { // para no marcar la entrada ni la salida
            maze[x][y] = Estado.Close;
            renderCache.push({y, x, estado: Estado.Close});
        }
        expandido++;

        if (!(x === salida.x && y === salida.y)) {
            const vecinos = generarSucesores(maze, x, y);
            vecinos.forEach(vecino => {
                const {x, y} = vecino;
                if (visitado[x] === undefined) visitado[x] = {};
                if (visitado[x][y] === undefined) visitado[x][y] = Estado.Default;


                if (visitado[x][y] === Estado.Default && g > 0) {

                    maze[x][y] = Estado.Open;
                    renderCache.push({y, x, estado: Estado.Open});
                    pila.push({x, y, g: g - 1});
                }
            });

        } else {
            break;
        }
    }

}

///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
/**
 *  HEURISTICAS
 */
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////

/**
 * suma de los valores absolutos de las diferencias en las coordenadas x e y del objetivo y las coordenadas x e y de la
 * celda actual.
 * @param actual la celda actual.
 * @param objetivo la celda objetivo.
 */
const distanciaManhattan = (actual = {x: 0, y: 0}, objetivo = {x: 0, y: 0}) => {
    return Math.abs(actual.x - objetivo.x) + Math.abs(actual.y - objetivo.y);
}

/**
 * máximo de los valores absolutos de las diferencias en las coordenadas x e y del objetivo y las coordenadas x e y de
 * la celda actual, respectivamente.
 * @param actual la celda actual.
 * @param objetivo la celda objetivo.
 * @param d el tamanho de la celda.
 * @param d2 el tamanho de la diagonal entre los nodos.
 */
const distanciaDiagonal = (actual = {x: 0, y: 0}, objetivo = {x: 0, y: 0}, d = c, d2 = Math.sqrt(2)) => {
    const dx = Math.abs(actual.x - objetivo.x);
    const dy = Math.abs(actual.y - objetivo.y);

    return d * (dx + dy) + (d2 - 2 * d) * Math.min(dx, dy);
}

/**
 * distancia entre la celda actual y la celda objetivo usando la fórmula de distancia.
 * @param actual la celda actual.
 * @param objetivo la celda objetivo.
 */
const distanciaEuclidiana = (actual = {x: 0, y: 0}, objetivo = {x: 0, y: 0}) => {
    return Math.sqrt(Math.pow(actual.x - objetivo.x, 2) + Math.pow(actual.y - objetivo.y, 2));
}


///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
/**
 *  GENERADOR DE CANVAS
 */
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////

const setupCanvas = (width, height) => {
    const canvas = document.createElement('canvas');
    canvas.setAttribute('width', width);
    canvas.setAttribute('height', height);
    canvas.style.border = '1px dashed black';
    return canvas;
};

const draw = (maze = [[]], ctx) => {
    for (let y = 0; y < maze.length; y++) {
        for (let x = 0; x < maze[0].length; x++) {
            ctx.fillStyle = ["red", "blue", "white", "black", "aqua", "teal"][maze[y][x]];
            ctx.fillRect(x * c, y * c, c, c)

        }
    }
};


const render = () => {
    if (renderCache.length > 0) {
        const {x, y, estado} = renderCache.shift();
        ctx.fillStyle = styles[estado];
        ctx.fillRect(y * c, x * c, c, c);
    }
}

///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
/**
 *  GENERADOR DE MATRIZ
 */
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
/**
 * termina de colocar caminos en las celdas visitadas
 * @param maze
 */
const cleanMaze = (maze) => {
    for (let i = 1; i + 2 <= maze.length; i = i + 2) {
        for (let j = 1; j + 2 <= maze[i].length; j = j + 2) {
            maze[i][j] = Estado.Camino;
        }
    }
};

/**
 * imprime la matriz en consola.
 * @param array
 */
const printMaze = (array = [[]]) => {
    for (let i = 0; i < array.length; i++) {
        console.log(array[i].toString());
    }
}

/**
 * crea una matriz de celdas no visitadas.
 * @param fila
 * @param columna
 * @returns {any[][]}
 */
const initMaze = (fila = 0, columna = 0) => {
    return Array((fila * 2) + 1).fill(null).map(() => Array((columna * 2) + 1).fill(Estado.Pared));     // [n*2+1]*[m*2+1]
}

const perfectMaze = (maze) => {
    for (let i = 1; i + 2 <= maze.length; i = i + 2) {
        for (let j = 1; j + 2 <= maze[i].length; j = j + 2) {
            maze[i][j] = Estado.Default;
        }
    }
}

const randomMaze = (maze = [[]], distribucion = 3) => {
    for (let i = 0; i < maze.length; i++) {
        for (let j = 0; j < maze[1].length; j++) {
            const rand = Math.floor(Math.random() * (distribucion));
            maze[i][j] = rand === 0 ? Estado.Pared : Estado.Camino;
        }
    }
    maze[0][0] = 0
    maze[maze.length - 1][maze[0].length - 1] = 1
}

/**
 * crea la el laberinto
 * @param maze
 * @param x
 * @param y
 */
const generateMaze = (maze, x = 1, y = 1) => {
    maze[x][y] = Estado.Visitado;

    let vecino;
    while ((vecino = vecinoNoVisitado(maze, x, y)) !== null) {
        const {x: filaVecino, y: columnaVecino, direction} = vecino;
        removerPared(maze, x, y, direction)
        generateMaze(maze, filaVecino, columnaVecino);
    }
}


/**
 * Devuelve y quita un objeto randomico del arreglo
 *
 * @param arr
 * @returns {*[]}
 */
const getRandomItem = (arr) => {
    return (arr.splice((Math.random() * arr.length) | 0, 1)[0]);
}

/**
 * devuelve la posicion de la celda de un vecino, y hacia que direccion se encuentra
 * @param maze
 * @param x
 * @param y
 * @returns {*[]|null}
 */
const vecinoNoVisitado = (maze = [[]], x = 0, y = 0) => {
    const vecinos = [];
    if (x > 1 && maze[x - 2][y] === Estado.Default) {   //verificar arriba
        vecinos.push({x: x - 2, y: y, direction: Direction.Up});
    }

    if (y > 1 && maze[x][y - 2] === Estado.Default) {    // verificar izquierda
        vecinos.push({x: x, y: y - 2, direction: Direction.Left});
    }

    if (x < maze.length - 2 && maze[x + 2][y] === Estado.Default) {  // verificar abajo
        vecinos.push({x: x + 2, y: y, direction: Direction.Down});
    }

    if (y < maze[x].length - 2 && maze[x][y + 2] === Estado.Default) {// verifico derecha
        vecinos.push({x: x, y: y + 2, direction: Direction.Right});
    }
    return vecinos.length > 0 ? getRandomItem(vecinos) : null;
}

/**
 * remueve la pared entre 2 celdas
 * @param maze
 * @param x
 * @param y
 * @param pared
 */
const removerPared = (maze = [[]], x = 0, y = 0, pared = Direction.Default) => {
    if (pared === Direction.Up) {
        maze[x - 1][y] = Estado.Camino;
    } else if (pared === Direction.Left) {
        maze[x][y - 1] = Estado.Camino;
    } else if (pared === Direction.Down) {
        maze[x + 1][y] = Estado.Camino;
    } else if (pared === Direction.Right) {
        maze[x][y + 1] = Estado.Camino;
    }
}

const round = (a) => {
    return Math.round((a + Number.EPSILON) * 100) / 100
}

setInterval(render, 1);
main();
if (renderCache.length > 0) {
    console.log("cantidad de nodos expandidos", renderCache.length);
}

console.log("cantidad de nodos cerrados", expandido);