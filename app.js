require('dotenv').config();
const express = require('express');

const app = express();
const port = process.env.PORT;

// statics
app.use(express.static(__dirname + '/public'));


app.get('/', (req, res) => {
    res.sendFile('index.html')
});


app.get('*', (req, res) => {
    res.send('404 page not found');
});

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
});